import Vue, { nextTick } from 'vue';
import VueApollo from 'vue-apollo';
import { GlCollapsibleListbox } from '@gitlab/ui';
import { convertToGraphQLId } from '~/graphql_shared/utils';
import { TYPENAME_PROJECT, TYPENAME_GROUP } from '~/graphql_shared/constants';
import { shallowMountExtended } from 'helpers/vue_test_utils_helper';
import createMockApollo from 'helpers/mock_apollo_helper';
import waitForPromises from 'helpers/wait_for_promises';
import getGroups from 'ee/security_orchestration/graphql/queries/get_groups_for_policies.query.graphql';
import getGroupProjects from 'ee/security_orchestration/graphql/queries/get_group_projects.query.graphql';
import GroupProjectsDropdown from 'ee/security_orchestration/components/group_projects_dropdown.vue';

describe('GroupProjectsDropdown', () => {
  let wrapper;
  let requestHandlers;

  const GROUP_FULL_PATH = 'gitlab-org';

  const generateMockNode = (ids) =>
    ids.map((id) => ({
      id: convertToGraphQLId(TYPENAME_PROJECT, id),
      name: `${id}`,
      fullPath: `project-${id}-full-path`,
      repository: { rootRef: 'main' },
    }));

  const generateMockGroups = (ids) =>
    ids.map((id) => ({
      id: convertToGraphQLId(TYPENAME_GROUP, id),
      name: `${id}`,
    }));

  const defaultNodes = generateMockNode([1, 2]);
  const defaultGroups = generateMockGroups([1, 2]);

  const defaultNodesIds = defaultNodes.map(({ id }) => id);
  const defaultGroupIds = defaultGroups.map(({ id }) => id);

  const mapItems = (items) => items.map(({ id, name }) => ({ value: id, text: name }));

  const defaultPageInfo = {
    __typename: 'PageInfo',
    hasNextPage: false,
    hasPreviousPage: false,
    startCursor: null,
    endCursor: null,
  };

  const mockApolloHandlers = (
    nodes = defaultNodes,
    hasNextPage = false,
    groups = defaultGroups,
  ) => {
    return {
      getGroups: jest.fn().mockResolvedValue({
        data: {
          group: {
            id: '1',
            name: 'gitlab-policies',
            descendantGroups: {
              nodes: groups,
              pageInfo: { ...defaultPageInfo, hasNextPage },
            },
          },
        },
      }),
      getGroupProjects: jest.fn().mockResolvedValue({
        data: {
          id: 1,
          group: {
            id: 2,
            projects: {
              nodes,
              pageInfo: { ...defaultPageInfo, hasNextPage },
            },
          },
        },
      }),
    };
  };

  const createMockApolloProvider = (handlers) => {
    Vue.use(VueApollo);

    requestHandlers = handlers;
    return createMockApollo([
      [getGroupProjects, requestHandlers.getGroupProjects],
      [getGroups, requestHandlers.getGroups],
    ]);
  };

  const createComponent = ({
    propsData = {},
    handlers = mockApolloHandlers(),
    stubs = {},
  } = {}) => {
    wrapper = shallowMountExtended(GroupProjectsDropdown, {
      apolloProvider: createMockApolloProvider(handlers),
      propsData: {
        groupFullPath: GROUP_FULL_PATH,
        ...propsData,
      },
      stubs,
    });
  };

  const findDropdown = () => wrapper.findComponent(GlCollapsibleListbox);

  describe.each`
    groupsOnly | items            | expectedText
    ${false}   | ${defaultNodes}  | ${'Select projects'}
    ${true}    | ${defaultGroups} | ${'Select groups'}
  `('selection', ({ groupsOnly, items, expectedText }) => {
    beforeEach(() => {
      createComponent({
        propsData: {
          groupsOnly,
        },
      });
    });

    it('should render loading state', () => {
      expect(findDropdown().props('loading')).toBe(true);
    });

    it('should load items', async () => {
      await waitForPromises();
      expect(findDropdown().props('loading')).toBe(false);
      expect(findDropdown().props('items')).toEqual(mapItems(items));
    });

    it('should select items', async () => {
      const [{ id }] = items;

      await waitForPromises();
      findDropdown().vm.$emit('select', [id]);
      expect(wrapper.emitted('select')).toEqual([[[items[0]]]]);
    });

    it('renders default text when loading', () => {
      expect(findDropdown().props('toggleText')).toBe(expectedText);
    });
  });

  it.each`
    groupsOnly | items
    ${false}   | ${defaultNodes}
    ${true}    | ${defaultGroups}
  `('should select full items with full id format', async ({ groupsOnly, items }) => {
    createComponent({
      propsData: {
        useShortIdFormat: false,
        groupsOnly,
      },
    });

    const [{ id }] = items;

    await waitForPromises();
    findDropdown().vm.$emit('select', [id]);
    expect(wrapper.emitted('select')).toEqual([[[items[0]]]]);
  });

  describe.each`
    groupsOnly | ids                | expectedText
    ${false}   | ${defaultNodesIds} | ${'All projects'}
    ${true}    | ${defaultGroupIds} | ${'All groups'}
  `('selected items', ({ groupsOnly, ids, expectedText }) => {
    const type = groupsOnly ? 'groups' : 'projects';

    beforeEach(() => {
      createComponent({
        propsData: {
          selected: ids,
          groupsOnly,
        },
      });
    });

    it(`should be possible to preselect ${type}`, async () => {
      await waitForPromises();
      expect(findDropdown().props('selected')).toEqual(ids);
    });

    it(`renders all ${type} selected text when`, async () => {
      await waitForPromises();
      expect(findDropdown().props('toggleText')).toBe(expectedText);
    });
  });

  describe('selected items that does not exist', () => {
    it.each`
      groupsOnly | expectedText
      ${false}   | ${'Select projects'}
      ${true}    | ${'Select groups'}
    `(
      'renders default placeholder when selected projects do not exist',
      async ({ groupsOnly, expectedText }) => {
        createComponent({
          propsData: {
            selected: ['one', 'two'],
            groupsOnly,
          },
        });

        await waitForPromises();
        expect(findDropdown().props('toggleText')).toBe(expectedText);
      },
    );

    it('filters selected projects that does not exist', async () => {
      createComponent({
        propsData: {
          selected: ['one', 'two'],
          useShortIdFormat: false,
        },
      });

      await waitForPromises();
      findDropdown().vm.$emit('select', [defaultNodesIds[0]]);

      expect(wrapper.emitted('select')).toEqual([[[defaultNodes[0]]]]);
    });
  });

  describe.each`
    type         | groupsOnly | ids                | items            | handlers
    ${'project'} | ${false}   | ${defaultNodesIds} | ${defaultNodes}  | ${mockApolloHandlers()}
    ${'group'}   | ${true}    | ${defaultGroupIds} | ${defaultGroups} | ${mockApolloHandlers([], false, defaultGroups)}
  `('select single $type', ({ type, groupsOnly, ids, items, handlers }) => {
    it('support single selection mode', async () => {
      createComponent({
        propsData: {
          multiple: false,
          groupsOnly,
        },
        handlers,
      });

      await waitForPromises();

      findDropdown().vm.$emit('select', ids[0]);
      expect(wrapper.emitted('select')).toEqual([[items[0]]]);
    });

    it(`should render single selected ${type}`, async () => {
      createComponent({
        propsData: {
          multiple: false,
          selected: ids[0],
          groupsOnly,
        },
        handlers,
      });

      await waitForPromises();

      expect(findDropdown().props('selected')).toEqual(ids[0]);
    });
  });

  describe('when there is more than a page of projects', () => {
    describe('when bottom reached on scrolling', () => {
      describe('groups', () => {
        it('makes a query to fetch more groups', async () => {
          createComponent({
            propsData: { groupsOnly: true },
            handlers: mockApolloHandlers([], true, []),
          });
          await waitForPromises();

          findDropdown().vm.$emit('bottom-reached');
          expect(requestHandlers.getGroupProjects).toHaveBeenCalledTimes(0);
          expect(requestHandlers.getGroups).toHaveBeenCalledTimes(2);
        });

        it.each`
          hasNextPage | expectedText
          ${true}     | ${'1, 2'}
          ${false}    | ${'All groups'}
        `(
          'selects all groups only when all groups loaded',
          async ({ hasNextPage, expectedText }) => {
            createComponent({
              propsData: {
                selected: defaultGroupIds,
                groupsOnly: true,
              },
              handlers: mockApolloHandlers([], hasNextPage, defaultGroups),
            });

            await waitForPromises();

            expect(findDropdown().props('toggleText')).toBe(expectedText);
          },
        );
      });

      describe('projects', () => {
        it('makes a query to fetch more projects', async () => {
          createComponent({ handlers: mockApolloHandlers([], true) });
          await waitForPromises();

          findDropdown().vm.$emit('bottom-reached');
          expect(requestHandlers.getGroupProjects).toHaveBeenCalledTimes(2);
          expect(requestHandlers.getGroups).toHaveBeenCalledTimes(0);
        });

        it.each`
          hasNextPage | expectedText
          ${true}     | ${'1, 2'}
          ${false}    | ${'All projects'}
        `(
          'selects all projects only when all projects loaded',
          async ({ hasNextPage, expectedText }) => {
            createComponent({
              propsData: {
                selected: defaultNodesIds,
              },
              handlers: mockApolloHandlers(defaultNodes, hasNextPage),
            });

            await waitForPromises();

            expect(findDropdown().props('toggleText')).toBe(expectedText);
          },
        );
      });

      describe('when the fetch query throws an error', () => {
        it.each`
          groupsOnly | event
          ${false}   | ${'projects-query-error'}
          ${true}    | ${'groups-query-error'}
        `('emits an error event', async ({ groupsOnly, event }) => {
          createComponent({
            propsData: {
              groupsOnly,
            },
            handlers: {
              getGroupProjects: jest.fn().mockRejectedValue({}),
            },
          });
          await waitForPromises();
          expect(wrapper.emitted(event)).toHaveLength(1);
        });
      });
    });

    describe('when a query is loading a new page of projects', () => {
      it.each`
        groupsOnly | handlers
        ${false}   | ${mockApolloHandlers([], true)}
        ${true}    | ${mockApolloHandlers([], true, [])}
      `('should render the loading spinner', async ({ groupsOnly, handlers }) => {
        createComponent({ propsData: { groupsOnly }, handlers });
        await waitForPromises();

        findDropdown().vm.$emit('bottom-reached');
        await nextTick();

        expect(findDropdown().props('loading')).toBe(true);
      });
    });
  });

  describe('full id format', () => {
    it.each`
      groupsOnly | ids
      ${false}   | ${defaultNodesIds}
      ${true}    | ${defaultGroupIds}
    `('should render selected ids in full format', async ({ groupsOnly, ids }) => {
      createComponent({
        propsData: {
          selected: ids,
          useShortIdFormat: false,
          groupsOnly,
        },
      });

      await waitForPromises();

      expect(findDropdown().props('selected')).toEqual(ids);
    });
  });

  describe('validation', () => {
    it.each([false, true])('renders default dropdown when validation passes', (groupsOnly) => {
      createComponent({
        propsData: {
          state: true,
          groupsOnly,
        },
      });

      expect(findDropdown().props('variant')).toEqual('default');
      expect(findDropdown().props('category')).toEqual('primary');
    });

    it.each([false, true])('renders danger dropdown when validation passes', (groupsOnly) => {
      createComponent({
        propsData: {
          groupsOnly,
        },
      });

      expect(findDropdown().props('variant')).toEqual('danger');
      expect(findDropdown().props('category')).toEqual('secondary');
    });
  });

  describe('select all', () => {
    describe.each(['groups', 'projects'])('items', (itemType) => {
      it(`selects all ${itemType}`, async () => {
        const groupsOnly = itemType === 'groups';
        const nodes = groupsOnly ? defaultGroups : defaultNodes;

        createComponent({
          propsData: {
            groupsOnly,
          },
        });
        await waitForPromises();

        findDropdown().vm.$emit('select-all');

        expect(wrapper.emitted('select')).toEqual([[nodes]]);
      });

      it('resets all groups', async () => {
        createComponent({
          propsData: {
            groupsOnly: true,
          },
        });

        await waitForPromises();

        findDropdown().vm.$emit('reset');

        expect(wrapper.emitted('select')).toEqual([[[]]]);
      });
    });
  });

  describe('selection after search', () => {
    describe('groups', () => {
      it('should add projects to existing selection after search', async () => {
        const moreNodes = generateMockGroups([1, 2, 3, 44, 444, 4444]);
        createComponent({
          propsData: {
            selected: defaultGroupIds,
            groupsOnly: true,
          },
          handlers: mockApolloHandlers([], false, moreNodes),
          stubs: {
            GlCollapsibleListbox,
          },
        });

        await waitForPromises();

        expect(findDropdown().props('selected')).toEqual(defaultGroupIds);

        findDropdown().vm.$emit('search', '4');
        await waitForPromises();

        expect(requestHandlers.getGroups).toHaveBeenCalledWith({
          search: '4',
          fullPath: GROUP_FULL_PATH,
        });
        expect(requestHandlers.getGroupProjects).toHaveBeenCalledTimes(0);

        await waitForPromises();

        await wrapper.findByTestId(`listbox-item-${moreNodes[3].id}`).vm.$emit('select', true);

        expect(wrapper.emitted('select')).toEqual([[[...defaultGroups, moreNodes[3]]]]);
      });
    });

    describe('projects', () => {
      it('should add projects to existing selection after search', async () => {
        const moreNodes = generateMockNode([1, 2, 3, 44, 444, 4444]);
        createComponent({
          propsData: {
            selected: defaultNodesIds,
          },
          handlers: mockApolloHandlers(moreNodes),
          stubs: {
            GlCollapsibleListbox,
          },
        });

        await waitForPromises();

        expect(findDropdown().props('selected')).toEqual(defaultNodesIds);

        findDropdown().vm.$emit('search', '4');
        await waitForPromises();

        expect(requestHandlers.getGroups).toHaveBeenCalledTimes(0);
        expect(requestHandlers.getGroupProjects).toHaveBeenCalledWith({
          fullPath: GROUP_FULL_PATH,
          projectIds: null,
          search: '4',
        });

        await waitForPromises();
        await wrapper.findByTestId(`listbox-item-${moreNodes[3].id}`).vm.$emit('select', true);

        expect(wrapper.emitted('select')).toEqual([[[...defaultNodes, moreNodes[3]]]]);
      });
    });
  });
});
